//
//  ViewController+RecipeInfoVC.h
//  Recipes
//
//  Created by Michael Childs on 2/7/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController (RecipeInfoVC)

@property (nonatomic, strong) NSDictionary* recipeInformationDictionary;

@end
