//
//  ViewController.h
//  Recipes
//
//  Created by Michael Childs on 2/7/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecipeInfoViewController.h"

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

{
    UITableView* myTableView;
    NSArray* array;
}
@end

