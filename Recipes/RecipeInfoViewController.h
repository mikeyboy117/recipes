//
//  RecipeInfoViewController.h
//  Recipes
//
//  Created by Michael Childs on 2/7/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeInfoViewController : UIViewController

@property (nonatomic, strong) NSDictionary* recipeInformationDictionary;

@end
