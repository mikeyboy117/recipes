//
//  ViewController.m
//  Recipes
//
//  Created by Michael Childs on 2/7/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    array = [[NSArray alloc]initWithContentsOfFile:path];
    
    if (array.count > 0) {
        [ud setObject:array forKey:@"array"];
        [ud synchronize];
    } else {
        array = [ud objectForKey:@"array"];
    }
    
    myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height - 20) style:UITableViewStylePlain];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    myTableView.backgroundColor = [UIColor whiteColor];
    
    
    [self.view addSubview:myTableView];
    
    self.navigationController.navigationBar.topItem.title = @"Recipes!"; // display title in navigationBar
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.537 green:0.886 blue:0.867 alpha:1.00];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier = @"cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.backgroundColor = [UIColor lightGrayColor];
    //    tableView.rowHeight = 100;
    tableView.backgroundColor = [UIColor whiteColor];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    
    NSDictionary* dictionary = [array objectAtIndex:indexPath.row];
    
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, cell.frame.size.height - 10, cell.frame.size.height -10)];
    iv.backgroundColor = [UIColor grayColor];
    iv.image = [UIImage imageNamed:dictionary[@"recipeImg"]];
    [cell addSubview:iv];
    
    UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(iv.frame.origin.x + iv.frame.size.width +10, 0, cell.frame.size.width - iv.frame.origin.x - iv.frame.size.width - 10, cell.frame.size.height)];
    lbl.text = dictionary[@"name"];
    [cell addSubview:lbl];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    RecipeInfoViewController* rivc = [[RecipeInfoViewController alloc]init];
    rivc.recipeInformationDictionary = [array objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:rivc animated:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
