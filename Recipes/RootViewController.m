//
//  RootViewController.m
//  Recipes
//
//  Created by Michael Childs on 2/7/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "RootViewController.h"
#import "ViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ViewController* vc = [ViewController new];
    
    UINavigationController* navController = [[UINavigationController alloc]initWithRootViewController:vc];
    [self addChildViewController:navController];
    [self.view addSubview:navController.view];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
