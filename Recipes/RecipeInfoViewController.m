//
//  RecipeInfoViewController.m
//  Recipes
//
//  Created by Michael Childs on 2/7/16.
//  Copyright © 2016 Michael Childs. All rights reserved.
//

#import "RecipeInfoViewController.h"
#import "ViewController.h"
@interface RecipeInfoViewController ()

@end

@implementation RecipeInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    int width = self.view.frame.size.width;
    int height = self.view.frame.size.height;
    
    UIScrollView* sv = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, width, height)];
    
    self.view.backgroundColor = [UIColor darkGrayColor];
    sv.contentSize = CGSizeMake(320, height);
    sv.scrollEnabled = YES;
    
    
    [self.view addSubview:sv];
    
    //    NSLog(@"%@", [self.recipeInformationDictionary objectForKey:@"name"]);
    //    NSLog(@"%@", [self.recipeInformationDictionary objectForKey:@"recipe"]);
    //    NSLog(@"%@", [self.recipeInformationDictionary objectForKey:@"recipeImg"]);
    
    
    //UIImageview for the Image
    
    UIImageView* recipeImage = [[UIImageView alloc]init];
    recipeImage.frame = CGRectMake(0, 0, sv.frame.size.width, 300);
    recipeImage.contentMode = UIViewContentModeScaleAspectFit;
    recipeImage.image = [UIImage imageNamed:[self.recipeInformationDictionary objectForKey:@"recipeImg"]];
    
    [sv addSubview:recipeImage];
    
    self.navigationController.topViewController.title =[self.recipeInformationDictionary objectForKey:@"name"]; // display recipe name within navigationBar
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.537 green:0.886 blue:0.867 alpha:1.00];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    
    
    UITextView* recipeText = [[UITextView alloc]initWithFrame:CGRectMake(0, recipeImage.frame.origin.y + recipeImage.frame.size.height, width, height / 2)];
    recipeText.textAlignment = NSTextAlignmentCenter;
    recipeText.text = [self.recipeInformationDictionary objectForKey:@"recipe"];
    recipeText.backgroundColor = [UIColor darkGrayColor];
    recipeText.textColor = [UIColor whiteColor];
    recipeText.scrollEnabled = NO;
    recipeText.editable = NO;
    [recipeText sizeToFit];
    
    [sv addSubview:recipeText];
    
    [sv setContentSize:CGSizeMake(width, recipeText.frame.origin.y + recipeText.frame.size.height)];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
